# Search Protocol - Group 70

Members : Ruchi Pandey, Victoria Ali, Kiran Maria Abraham

Search string

~~~~~ {.searchstring }
(((Web OR Internet) AND Accessib*) AND ("Standard*" OR Regulation*) AND ( "Public Sector*" OR Government))
~~~~~

Number of papers: 83
After criteria: 25

Inclusion criteria:

1.Journal and conference papers are included.
2.The papers must answer the research question.
3.The papers must be in English.

Exclusion criteria:

1.Assistive technology not meant for web accessibility (for example: joystick).
2.Assistive technology tools (for example: text-to-speech software).
