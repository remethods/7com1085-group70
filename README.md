Points for Topic:

TOPICS and things to consider. 

Understanding the impact of covid 19 on accessibility.

How has covid affected persons with disabilities?

Has covid made it harder for those with disabilities to access public sector services?

How has the global pandemic affected accessibility?

Everything that was added were some of the most popular search results found which also brought up various articles and papers. 


Points about our research question:

In the UK there are approximately 2,000 of 44,000 websites covering education, health, welfare, local and central government services.

· For people with disabilities, public health resources are often of greater importance; they additionally provide disability context-specific information.

. These resources provide key disability-related information for people with disabilities, constituting 15% of the overall world population.

· Web accessibility requirements should be met at an acceptable level.

. Accessibility guidelines (i.e., accessibility metrics) outline a set of technical requirements for making any digital content accessible for diverse users.

· The evaluation was conducted using the following automatic accessibility testing tools: AChecker, WAVE, W3C HTML Validator

. These accessibility guidelines ensures that there are no critical barriers to accessing the information in public sector websites.
